/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#include <ForthyShell.h>
#include "main.h"
#include <Arduino.h>
#include <Product.h>
#include <StringBuffer.h>

using namespace ForthyShell;

Product currentProduct = Product::Default;

int debugShowFlags = 0;

void doHandshakeStart(InterpretationContext& c) {
	c.print("Starting machine handshake.\n");
	machine.startHandshake(false);
}

void doObfHsStart(InterpretationContext& c) {
	c.print("Starting obfuscated machine handshake.\n");
	machine.startHandshake(true);
}

void doGetHandshakeState(InterpretationContext& c) {
	c.push(machine.isHandshakeDone());
}

void doSendRawObfuscated(InterpretationContext& c) {
	machine.sendRaw(c.readToken(""), true); // send raw string until end of line
}

void doSendRaw(InterpretationContext& c) {
	machine.sendRaw(c.readToken(""), false); // send raw string until end of line
}

#define PUSH(c) \
	[](InterpretationContext& i) { \
		i.push((int) (c)); \
	}

#define COFFEE_VAR2(nam, cv) {nam, PUSH(&currentProduct.cv)}
#define COFFEE_VAR(cv) COFFEE_VAR2(#cv, cv)

static constexpr Word words[]{
	{"hstart", doHandshakeStart},
	{"ohstart", doObfHsStart},
	{"hstate@", doGetHandshakeState},
	{"$", doSendRawObfuscated},
	{"%", doSendRaw},
	{"default", [](InterpretationContext& c){currentProduct = Product::Default;}},
	COFFEE_VAR2("product", productCode),
	COFFEE_VAR2("grind", finenessOfGrinding),
	COFFEE_VAR(strength),
	COFFEE_VAR2("amount", coffeeAmount),
	COFFEE_VAR(milkAmount),
	COFFEE_VAR(foamAmount),
	COFFEE_VAR(temperature),
	COFFEE_VAR(strokes),
	COFFEE_VAR(f09),
	COFFEE_VAR(bypassAmount),
	COFFEE_VAR2("pause", milkPause),
	COFFEE_VAR(f12),
	COFFEE_VAR(f13),
	COFFEE_VAR(temperatures),
	{"coffee.", [](InterpretationContext& c){ 
		StringBuffer sb(32);
		currentProduct.format(sb);
		c.print(sb.get());
	}},
	{"brew", [](InterpretationContext& c){
		machine.startProduction(currentProduct);
	}},
	{"showflags", PUSH(&debugShowFlags)},
	{"flags.", [](InterpretationContext& c){
		unsigned long flags = machine.getFlags();
		StringBuffer b(12);
		b.appendHex(flags, 5);
		c.print(b.get());
	}}
};

static constexpr Dictionary d(words, sizeof(words) / sizeof(Word), &Dictionary::Default);
Interpreter i(d);

void prompt(PrintCallback pc)
{
	pc("\nok ");
}

void executePromptLine(const char* line, PrintCallback pc)
{
	Error e = i.execute(line, pc);
	if (e != Error::OK) {
		pc("error: ");
		pc(e.getMessage());
		const char* i = e.getInfo();
		if (i) {
			pc(": ");
			pc(i);
		}
	}
	prompt(pc);
}


