/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#include "main.h"
#include "config.h"
#include <ArduinoOTA.h>

void setupOTAUpdate()
{
	ArduinoOTA.setHostname(conf::wifi::hostname);
	ArduinoOTA.onStart([](){
			Serial.write("\n\n!!! OTA flashing started\n");
			});
	ArduinoOTA.onEnd([](){
			Serial.write("OTA flashing finished.\n");
			});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total){
			Serial.write("progress: ");
			Serial.print(progress);
			Serial.write(" / ");
			Serial.println(total);
			});
	ArduinoOTA.onError([](ota_error_t error) {
			Serial.write("Error ");
			Serial.print((unsigned int) error);
			Serial.write(" : ");
			if (error == OTA_AUTH_ERROR) {
				Serial.println("OTA_AUTH_ERROR");
			} else if (error == OTA_BEGIN_ERROR) {
				Serial.println("OTA_BEGIN_ERROR");
			} else if (error == OTA_CONNECT_ERROR) {
				Serial.println("OTA_CONNECT_ERROR");
			} else if (error == OTA_RECEIVE_ERROR) {
				Serial.println("OTA_RECEIVE_ERROR");
			} else if (error == OTA_END_ERROR) {
				Serial.println("OTA_END_ERROR");
			}
		});
	ArduinoOTA.begin(false);
	Serial.write("OTA ready, hostname ");
	Serial.println(ArduinoOTA.getHostname());
}

void updateOTAUpdate()
{
	ArduinoOTA.handle();
}

