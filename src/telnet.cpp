/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#include "main.h"
#include <JuraInterface.h>
#include <ESP8266WebServer.h>
#include <cstring>

WiFiServer telnet(23);
static WiFiClient client;
static StringBuffer buffer(128);
unsigned long lastFlags;

void messageDebugCallback(const JuraMessageView& msg)
{
	if (debugShowFlags <= 1) {
		if (strcmp(msg.getCommand(), "TF") == 0) {
			if (debugShowFlags == 0)
				return;
			auto flags = machine.getFlags();
			if (flags == lastFlags)
				return;
			lastFlags = flags;
		}
	}
	if (msg.isNewProtocol()) {
		client.print("@");
	}
	client.print(msg.getCommand());
	if (msg.hasFirstParameter()) {
		client.print(":");
	}
	client.print(msg.getFirstParameter());
	if (msg.hasSecondParameter()) {
		client.print(",");
	}
	client.print(msg.getSecondParameter());
	client.print("\n");
}

void writeToTelnet(const char* c)
{
	client.print(c);
}

void onConnect() {
	machine.attachDebug(messageDebugCallback);
	prompt(writeToTelnet);
}

void setupTelnet()
{
	telnet.begin();
	telnet.setNoDelay(true);
}

void updateTelnet()
{
	if (telnet.hasClient()) { // new client waiting?
		if (client.connected()) { // already have a client
			client.stop();
		}
		client = telnet.available();
		client.setNoDelay(true);
		client.flush();
		onConnect();
	}

	if (client.available()) { // data available for reading
		unsigned char c = client.read();
		if (c == '\n') {
			executePromptLine(buffer.get(), writeToTelnet);
			buffer.clear();
			return;
		}
		if (c < 0x80) { // filter out telnet control characters
			buffer.put(c);
		}
	}
}

