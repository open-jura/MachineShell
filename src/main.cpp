/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <JuraCoffeeMachine.h>
#include <JuraSerial.h>
#include <JuraObfuscation.h>
#include <JuraInterface.h>
#include <ESP8266WiFi.h>
#include <StatusLED.h>
#include "main.h"
#include "config.h"

StatusLED led(LED_BUILTIN, true);
SoftwareSerial serial(conf::serial::rxPin, conf::serial::txPin);
JuraSerial<SoftwareSerial> machineSerial(serial);
JuraObfuscator obfuscator(Permutation::P1, Permutation::P2);
JuraInterface machineInterface(machineSerial, obfuscator);
JuraCoffeeMachine machine(machineInterface);

void setupWifi()
{
	Serial.write("Connecting to ");
	Serial.write(conf::wifi::ssid);
	Serial.write("\n");
	WiFi.hostname(conf::wifi::hostname);
	WiFi.begin(conf::wifi::ssid, conf::wifi::password);
	WiFi.setAutoReconnect(true);
}

bool isNetworkSetupDone = false;

bool isWifiConnected()
{
	return WiFi.status() == WL_CONNECTED;
}

void updateNetworkSetup()
{
	if (!isNetworkSetupDone && isWifiConnected()) {
		Serial.write("WiFi connected (");
		Serial.print(WiFi.localIP().toString());
		Serial.write(")\nSetting up OTA update...\n");
		setupOTAUpdate();
		Serial.write("Setting up telnet...\n");
		setupTelnet();
		Serial.write("Network setup done.\n");
		isNetworkSetupDone = true;
	}
}

struct ledstate {
	uint8_t status;
	uint8_t num;
	bool operator !=(const ledstate& o) {
		return (status != o.status) || (num != o.num);
	}
};

ledstate getNewLEDState()
{
	if (!isWifiConnected()) {
		return {1, 4};
	}

	if (!isNetworkSetupDone) {
		return {3, 4};
	}

	if (!machine.isHandshakeDone()) {
		return {2, 4};
	}

	return {1, 1};
}

void updateLEDState()
{
	static ledstate old;

	ledstate n = getNewLEDState();
	if (n != old) {
		old = n;
		led.setStatus(n.status, n.num);
	}

	led.update();
}

void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	Serial.begin(115200);
	machineSerial.begin();

	setupWifi();
	if (conf::doHandshakeOnBoot) {
		Serial.write("Auto-starting machine handshake.\n");
		machine.startHandshake(false);
	}
}

void loop()
{
	updateNetworkSetup();
	if (isNetworkSetupDone) {
		updateTelnet();
		updateOTAUpdate();
	}
	machine.update();
	updateLEDState();
}

